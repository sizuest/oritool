# oritool

Web-tool für die Durchführung von Orientierungsfahrten mit folgenden Funktionen:
- Erfassen von Teilnehmern
- Definition von Lauf-Eigenschaften
- Definition von DKs und AKs
- Regisitrierung des Passierens von DKs und AKs durch Teilnehmer
- Auswertung